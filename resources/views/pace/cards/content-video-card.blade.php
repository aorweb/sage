{{--
Card used in testimony slider has options for author information and an image or video

Site: Pace Home page testimonial slider

Components
$content- string:
$author - string:
$author_sub_text - string:
$author_image - string:
$video - string:
$image - string:
 --}}
<div class="content-video-card @if(isset($small_font) && $small_font) small-font @endif">
    <div class="content-video-card__inner grid-x grid-margin-x">
        <div class="content-video-card__content cell small-12 large-6">
            <div class="content-video-card__text-container">
                @if(!empty($content))
                    <img class="quote" src="@asset('images/curly-quotes.png')"/>
                @endif
                <div class="content-video-card__text">
                    @if(isset($small_font) && $small_font)
                        <span>{!!$content ?? ''!!}</span>
                    @else
                        <h3>{!!$content ?? ''!!}</h3>
                    @endif
                </div>
            </div>
            @if($author || $author_sub_text || $author_image)
                <div class="content-video__author">
                    @if($author_image)
                        <div class="content-video__author-image" style="background-image: url('{{$author_image}}')"></div>
                    @endif
                    <div class="content-video__author-author">
                        <h6 class="heading">{!!$author ?? ''!!}</h6>
                        <span class="heading-small">{!!$author_sub_text ?? ''!!}</span>
                    </div>
                </div>
            @endif
        </div>
        <div class="content-video-card__video-container cell small-12 large-6">
            @if($is_video && $video)
                <div class="content-video-card__video">
                    <video width="320" height="240" controls>
                        <source src="{{$video}}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
            @elseif(!$is_video && $image)
                <div class="content-video-card__image" style="background-image: url('{{$image}}')"></div>
            @endif
        </div>
    </div>
</div>