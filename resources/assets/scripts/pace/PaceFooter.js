import 'jquery';
import Cookies from 'js-cookie'

//yarn add js-cookie
//to add the package needed

export default function () {
    function init() {
        //Closes top alert and saves cookie
        $('.cookie-button').on('click', function(event){
            event.preventDefault();
            $('.footer-with-menu__cookie').hide();

            document.cookie = "footeralert=hide";
        })

        //if there is a cookie set don't show the bar at the top of the page
        if (Cookies.get("footeralert") == 'hide'){
            $('.footer-with-menu__cookie').hide();
        }
    }
    return { init };
}