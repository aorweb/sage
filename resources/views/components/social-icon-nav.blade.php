@foreach($items as $item)
  <a href="{{$item->url}}" target="_blank">

  @if(is_int(strpos($item->url,'facebook')))
    <object class="social-icon" type="image/svg+xml" data="@asset("images/social/fb.svg")"></object>
  @endif

  @if(is_int(strpos($item->url,'linkedin')))
    <object class="social-icon" type="image/svg+xml" data="@asset("images/social/linkedin.svg")"></object>
  @endif

  @if(is_int(strpos($item->url,'twitter')))
    <object class="social-icon" type="image/svg+xml" data="@asset("images/social/twitter.svg")"></object>
  @endif

  </a>
@endforeach

