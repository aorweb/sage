<header class="header-horizontal">
    @if($alert)
      <div class="header-horizontal__bar">
        <div class="grid-container">
          {!!$alert!!}
          <a class="alert-close" href="">
            <span class="material-icons-outlined">
              close
            </span>
          </a>
        </div>
      </div>
    @endif
    <div class="header-horizontal__inner grid-container">
      @if($header_logo)
        <div class="header-horizontal__logo-container">
          <a class="brand" href="{{ home_url('/') }}">
            <img src="{{ $header_logo }}" alt="{{ $site_name }}" />
          </a>
        </div>
      @endif
      <div class="header-horizontal__menu">
        @if (has_nav_menu('menu'))
          {!! App\menu() !!}
        @endif
      </div>
      <div class="header-horizontal__linkedin">
        @if($linkedin)
          <a href="{!!$linkedin!!}">
            <img src="@asset('images/linkedin-icon.svg')"/>
          </a>
        @endif
      </div>
      <div class="header-horizontal__phone">
        @if($phone_num)
          <a href="tel:{!!$phone_num!!}">{{$phone_num}}</a>
        @endif
      </div>
      <div class="header-horizontal__actions">

        <a href="/" class="menu-toggle">
          @include('components.menu-icon')
        </a>
      </div>
    </div>
  </header>

  @include('partials.site-nav')