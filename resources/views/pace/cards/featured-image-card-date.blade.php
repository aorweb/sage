{{--
Card with a featured image date and title.

Site: Pace Home page

Components
$link - string:
$image - string: featured image
$date - string: date for card to be formatted before entering
$content - string:
 --}}

<div class="featured-image-card cell small-12 medium-6 large-6 {{$color ?? ''}}">
    <a href="{{$link}}">
        <div class="featured-image-card__container">
            <div class="featured-image-card__image" style="{{$image ? 'background-image: url('.$image.')' : 'background-color: #1871cb'}}"></div>
            <div class="featured-image-card__inner" style="background-image: url(@asset('images/flower-background.svg'))">
                @if($date)
                    <div class="featured-image-card__date">
                        <span class="material-icons-outlined">schedule</span>
                        <span class="date">{{$date}}</span>
                    </div>
                @endif
                <div class="featured-image-card__title-container">
                    <h4>{{$content ?? ''}}</h4>
                </div>
            </div>
        </div>
    </a>
</div>
