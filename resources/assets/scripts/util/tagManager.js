function init(id, datalayer = 'dataLayer') {
    if (typeof id !== 'string' || id.length === 0) {
        /* eslint-disable no-console */
        console.warn(
            'Expected a Google Tag Manager ID. Received: "%s". ' +
            'Not loading Google Tag Manager.',
            id
        );
        return;
        /* eslint-enable no-console */
    }

    /* Google Tag Manager */
    ((w, d, s, l, i) => {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js',
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true; j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    }) (window, document, 'script', datalayer, id);
    /* End Google Tag Manager */
}

export default { init };
