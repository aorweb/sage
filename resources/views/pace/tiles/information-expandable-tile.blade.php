<div class="information-expandable-tile">
    <div class="information-expandable-tile__inner">
        <div class="information-expandable-tile__title">
            @if($icon)
                <img class="img" src="{!!$icon!!}"/>
            @endif
            <h4 class="heading">{!!$title ?? ''!!}</h4>
        </div>
        <span class="desc">{!!$desc ?? ''!!}</span>

        @if($expandables)
            <div class="information-expandable-tile__expandables">
                @foreach($expandables as $expandable)
                    <div class="information-expandable-tile__expandable">
                        <div class="information-expandable-tile__expandable-title">
                            <h6 class="heading">{!!$expandable['title'] ?? ''!!}</h6>
                            @if($expandable['description'])
                                <a href="" class="information-expandable-tile__link"><span class="material-icons">expand_more</span></a>
                            @endif
                        </div>
                        @if($expandable['description'])
                            <div class="information-expandable-tile__description">
                                {!!$expandable['description']!!}
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>