<div class="contact-card">
    <div class="contact-card__inner">
        <div class="grid-x">
            <div class="contact-card__content cell small-12 {{$image ? 'large-7' : 'large-12'}}">
                <div class="contact-card__background-image" style="background-image: url('@asset('images/canada-map.svg')"></div>
                <div class="contact-card__heading-container">
                    <h4 class="heading">{!!$desc ?? ''!!}</h4>
                </div>
                <div class="contact-card__info-container">
                    <div class="grid-x grid-margin-x">
                        <div class="content cell small-12 medium-6 large-12">
                            <h2 class="name">{!!$name!!}</h2>
                            @if($phone)
                                <h1 class="number"><a href="tel:{!!$phone!!}">{!!$phone!!}</a></h1>
                            @endif
                            @if($email)
                                <h1 class="email"><a href="mailto:{!!$email!!}">{!!$email!!}</a></h1>
                            @endif
                            @if($linkedin)
                                <a class="linkedin" href="{!!$linkedin!!}">
                                    <img src="@asset('images/linkedin-icon-white.svg')"/>
                                </a>
                            @endif
                        </div>
                        <div class="contact-card__image-small cell small-12 medium-6 large-12">
                            <img class="small-image" src="{{$image}}"/>
                        </div>
                    </div>
                </div>
            </div>
            @if($image)
                <div class="contact-card__image cell small-12 large-5" style="background-image: url('{{$image}}')"></div>
            @endif
        </div>
    </div>
</div>