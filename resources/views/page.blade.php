@extends('layouts.app')

@section('content')
  <div class="grid-container">
    @while(have_posts()) @php(the_post())
      @include('partials.page-header')
      @include('content.content-page')
    @endwhile
  </div>
@endsection
