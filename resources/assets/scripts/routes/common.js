import { toggleBodyScrollable } from '../util/helpers';
import FixedHeader from '../components/FixedHeader';
import HeaderToggle from '../components/HeaderToggle';
import Menu from '../components/Menu';
import Search from '../components/Search';
import Slider from '../components/Slider';
import Alert from '../components/Alert';
import NestedSubmenu from '../components/NestedSubmenu';
import Expandable from '../components/Expandable';
// import PaceFooter from '../pace/PaceFooter';
// import ExpandableInfoCard from '../pace/cards/expandable-info-card';


export default {
  init() {
    // JavaScript to be fired on all pages
    $(document).foundation();

    // DOM element constants
    const $body = $('body'),
    $header = $body.children('.header'),
    $headerBG = $header.children('.header__bg'),
    $overlay = $body.children('.page-overlay');

    // Initialize components
    let header = FixedHeader(),
        search = Search(),
        menu = Menu();

    //initalize slider
    let slider = Slider();
    slider.init();

    let headerToggle = HeaderToggle();
    headerToggle.init();

    let nestedSubmenu = NestedSubmenu();
    nestedSubmenu.init();

    let alert = Alert();
    alert.init();

    let expandable = Expandable();
    expandable.init();
    //import js for pace footer
    // let paceFooter = PaceFooter();
    // paceFooter.init();

    // let expandableInfoCard = ExpandableInfoCard();
    // expandableInfoCard.init();

    // Hide search and menu when the overlay is clicked
    $overlay.on('click', (event) => {
      search.toggle(false);
      menu.toggle(false);

      event.preventDefault();
    });

    // Hide menu panel when search panel is activated
    search.addCallback((active) => {
      if (active && menu.isActive()) {
          menu.toggle(false);
      }

      $body.toggleClass('panel-open', active);

      updateHeader();
      updateOverlay();
    });

    // Hide search panel when menu panel is activated
    menu.addCallback((active) => {
        if (active && search.isActive()) {
            search.toggle(false);
        }

        $body.toggleClass('panel-open', active);

        updateHeader();
        updateOverlay();
    });

    /**
     * Fade the overlay in or out based on whether the search or menu is active
     */
    function updateOverlay() {
      if (search.isActive() || menu.isActive()) {
          $overlay.stop().fadeIn(200);
      }
      else {
          $overlay.stop().fadeOut(200);
      }

      toggleBodyScrollable(!search.isActive() && !menu.isActive(), $body, [$body, $header, $headerBG]);
    }

    /**
     * Sets the fixed and hidden states for the header based on the provided menuActive value
     */
    function updateHeader() {
        let newState = { listening: true },
            panelActive = search.isActive() || menu.isActive();

        // Set fixed if menu is open and header not fixed
        if (panelActive) {
            newState.fixed = true;
            newState.hidden = false;
            newState.listening = false;
        }
        // Remove fixed if menu is closed and at the top of the page
        else if (!panelActive && header.state.distance === 0) {
            newState.fixed = false;
        }

        header.updateState(newState);
    }
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
