/**
 * Component: FixedHeader
 *
 * This component handles the state of the site's header bar. The header is hidden while scrolling
 * down the page and fixed to the top of the screen when scrolling up.
 */

import throttle from 'lodash.throttle';

export default function () {
    // DOM element constants
    const $body = $('body'),
          $header = $body.children('.header'),
          trigger = 200;

    // Initialize state variables
    let state = {
        distance: 0,
        fixed: false,
        hidden: false,
        animating: false,
        listening: true,
    };

    // Initialize scroll distance
    updateState({ distance: $(window).scrollTop() });

    // Create listeners
    $(window).on('scroll', throttle(handleScroll, 10)).scroll();
    $header.on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', (event) => {
        if ($(event.target).hasClass('header')) {
            handleTransition();
        }
    });

    /**
     * Handles the window scroll event and determines the state of the header based on the
     * window scroll position. Updates the distance variable.
     */
    function handleScroll() {
        let newDistance = $(window).scrollTop();

        if (state.listening) {
            if (!state.fixed && !state.animating && newDistance > trigger) {
                updateState({
                    fixed: true,
                    hidden: true,
                });
            }
            else if (state.fixed && newDistance > trigger && state.hidden !== newDistance > state.distance) {
                updateState({
                    hidden: newDistance > state.distance,
                    animating: true,
                });
            }
            else if (state.fixed || state.hidden) {
                let offset = parseFloat($header.css('top')) || 0;

                if (newDistance === 0 || (offset < 0 && newDistance < Math.abs(offset))) {
                    updateState({
                        fixed: false,
                        hidden: false,
                    });
                }
            }
        }

        updateState({ distance: newDistance });
    }

    /**
     * Handler for the transition end events for the header. Removes the animating class from
     * the header when the transition is complete.
     */
    function handleTransition() {
        updateState({ animating: false });
    }

    /**
     * Update the FixedHeader state variables and toggle classes on the elements
     * when necessary
     *
     * @param {object} newState  object with any new state variables
     */
    function updateState(newState = false) {
        if (newState) {
            // Tracks which state variables have changed
            let changed = [],
                wasScrolled = state.distance > 0;

            // Update state values from newState variable
            for (let key in state) {
                if (newState[key] !== undefined && newState[key] !== state[key]) {
                    state[key] = newState[key];
                    changed[key] = true;
                }
            }

            // Toggle classes based on changed states
            if (changed['fixed']) {
                $body.toggleClass('fixed-header', state.fixed);
            }

            if (changed['animating']) {
                $header.toggleClass('animating', state.animating);
            }

            if (changed['hidden']) {
                $header.toggleClass('hidden', state.hidden);
            }

            if (changed['distance'] && state.distance > 0 !== wasScrolled) {
                $body.toggleClass('scrolled', state.distance > 0);
            }
        }

        return state;
    }

    return { state, updateState };
}
