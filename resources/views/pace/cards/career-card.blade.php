<div class="career-card">
    <div class="career-card__inner">
        <div class="grid-x grid-margin-x">
            <div class="career-card__content cell small-12 {{$image ? 'large-6' : 'large-12'}}">
                <h2 class="heading"><?= __("We're hiring!", "sage")?></h2>
                @if($opening)
                    <span class="opening"><?= __("Opening", "sage")?> <strong>{!!$opening!!}</strong></span>
                @endif
                <p class="desc">{!!$desc ?? ''!!}</p>
                <a class="button" href="{!!$link!!}"><span><?= __("Apply Now", "sage")?></span></a>
            </div>
            @if($image)
                <div class="career-card__image cell small-12 large-6" style="background-image: url('{{$image}}')">
                </div>
            @endif
        </div>
    </div>
</div>