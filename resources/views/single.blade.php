@extends('layouts.app')

@section('content')
  <div class="grid-container">
    @while(have_posts()) @php(the_post())
      @include('content.content-single-'.get_post_type())
    @endwhile
  </div>
@endsection
