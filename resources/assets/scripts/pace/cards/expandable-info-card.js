import 'jquery';

export default function () {
    function init() {
        // var state = false;
        $('.expandable-info-card__more').on('click', function(event){
            event.preventDefault();

            var $descHeight = $(this).parents('.expandable-info-card').children('.expandable-info-card__description').height();
            var $cardHeight = $(this).parents('.expandable-info-card').height();


            if($(this).parents('.expandable-info-card').hasClass('active')) {
                $(this).parents('.expandable-info-card').removeClass('active');
                if(window.Foundation.MediaQuery.is('small only') || window.Foundation.MediaQuery.is('medium only')) {
                    $('.expandable-info-card').css('height', 500);
                } else if(window.Foundation.MediaQuery.is('large')) {
                    $('.expandable-info-card').css('height', 250);
                }

            } else {
                $('.expandable-info-card').removeClass('active');
                $('.expandable-info-card').css('height', $cardHeight);

                $(this).parents('.expandable-info-card').addClass('active');
                $(this).parents('.expandable-info-card').css('height',$descHeight + $cardHeight + 50);
            }


        });
    }
    return { init };
}