<?php

/**
 * Use this file for registering new Post Types and Taxonomies
 */

namespace App;

// add_action('init', function () {
//     register_post_type('custom', [ 'public' => true,]);
// });
register_post_type('posttype', [
    'labels' => [
        'name' => _x('Post Type', 'Post Type General Name', 'sage'),
        'menu_name' => _x('Post Type', 'Post Type Menu Item Name', 'sage'),
    ],
    'public' => true,
    'supports' => [
        'title',
        'editor',
        'excerpt',
        'revisions',
        'thumbnail',
        'page-attributes',
    ],
    'taxonomies'  => [
        'taxonomies',
    ],
    'show_in_rest' => true,
    'has_archive' => true,
    'hierarchical' => true,
    'menu_icon' => 'dashicons-admin-post',
    'rewrite' => [
        'with_front' => false,
    ],
    'query_var' => true,
]);

register_taxonomy('taxonomies', 'posttype', [
    'labels' => [
        'name' => _x('Taxonomies', 'Taxonomy General Name', 'sage'),
        'singular_name' => _x('Taxonomies', 'Taxonomy Singular Name', 'sage'),
        'search_items' =>  __('Search Taxonomies', 'sage'),
        'popular_items' => __('Popular Taxonomies', 'sage'),
        'all_items' => __('All Taxonomies', 'sage'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Edit Taxonomies', 'sage'),
        'update_item' => __('Update Taxonomies', 'sage'),
        'add_new_item' => __('Add New Taxonomies', 'sage'),
        'new_item_name' => __('New Taxonomies Name', 'sage'),
        'separate_items_with_commas' => __('Separate Taxonomies with commas', 'sage'),
        'add_or_remove_items' => __('Add or remove Taxonomies', 'sage'),
        'choose_from_most_used' => __('Choose from the most used Taxonomies', 'sage'),
        'menu_name' => __('Taxonomies', 'sage'),
    ],
    'hierarchical' => true,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'show_in_rest' => true,
    'rewrite' => [
        'slug' => 'taxonomies'
    ],
]);