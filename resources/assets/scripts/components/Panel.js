/**
 * Component: Panel
 *
 * This component handles the toggling of a panel/window element.
 */

/**
 * Panel component constructor. Finds elements and sets listeners.
 * @param {jQuery el} $target   the panel element to be toggled
 * @param {jQuery el} $trigger  the element used to toggle the panel
 */
export default function ($target, $trigger, defaultCallback = null) {
    let callbacks = defaultCallback ? [defaultCallback] : [];

    // Set state variables
    let state = {
        active: false,
    };

    // Initialize the on click listener for the button and overlay
    $trigger.on('click', (event) => {
        toggle();
        event.preventDefault();
    });

    /**
     * Toggles the class modifiers for the icon and nav window. Toggles the page overlay and
     * toggles the ability to scroll on the page
     *
     * @param {boolean} makeActive  is the menu to be made active
     */
    function toggle(makeActive = null) {
        state.active = makeActive === null ? !state.active : makeActive;

        $target.toggleClass('active', state.active);
        $trigger.toggleClass('active', state.active);

        // Call the callback function if it exists
        callbacks.forEach((callback) => {
            if (typeof callback === 'function') {
                callback(state.active);
            }
        });
    }

    /**
     * Sets the callback function that is called after the menu is toggled
     *
     * @param {function} newCallback  the callback function for the menu toggling
     */
    function addCallback(newCallback) {
        callbacks.push(newCallback);
    }

    /**
     * Returns the current active state of the menu
     *
     * @return {boolean}  active or inactive
     */
    function isActive() {
        return state.active;
    }

    return { toggle, addCallback, isActive };
}
