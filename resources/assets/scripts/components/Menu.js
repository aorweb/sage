/**
 * Component: Menu
 *
 * This component handles the toggling of the main menu.
 */

import Panel from '../components/Panel';

export default function() {
    // Define constants
    const $menu = $('.site-nav--menu'),
          $btn = $('.menu-toggle'),
          $icon = $btn.children('.menu-bars');

    // Create menu panel and assign callback function
    let menuPanel = Panel($menu, $btn, menuToggled);

    /**
     * Function that handles the menu being toggled. Updates the menu icon and triggers
     * an update for the header.
     *
     * @param {bool} active  the active state of the panel (true or false)
     */
    function menuToggled(active) {
        $icon.toggleClass('menu-bars--close', active);
    }

    return {
        toggle: menuPanel.toggle,
        isActive: menuPanel.isActive,
        addCallback: menuPanel.addCallback,
    };
}
