<footer class="footer-with-menu">
    <div class="footer-with-menu__inner grid-container">
        <div class="grid-x grid-margin-x">
            <div class="footer-with-menu__info-container cell small-12 large-5">
                <div class="footer-with-menu__info">
                    <div class="footer-with-menu__content-container">
                        <span class="phone">{{$phone_num ?? ''}}</span>
                        @if($footer_email)
                            <a href="mailto:{{$footer_email}}" class="email">{{$footer_email}}</a>
                        @endif
                    </div>
                </div>
                @if($linkedin)
                    <a class="footer-with-menu__linkedin" href="{{$linkedin}}">
                        <img class="icon" src="@asset('images/linkedin-icon.svg')"/>
                    </a>
                @endif
            </div>
            <div class="footer-with-menu__menu cell small-12 large-7">
                @if (has_nav_menu('footer_menu'))
                    {!! App\footer_menu() !!}
                @endif
            </div>
        </div>
    </div>
    <div class="footer-with-menu__copy-right">
        <div class="grid-container">
            <span class="text"><?= __('Copyright ©2021 PACE Atlantic CIC | All Rights Reserved','sage') ?></span>
            <a class="link" href="{!!$privacy_policy_link['url']!!}" target="{!!$privacy_policy_link['target']!!}">{!!$privacy_policy_link['title']!!}</a>
        </div>
    </div>
    <div class="footer-with-menu__cookie">
        <div class="grid-container">
            <img class="icon" src="@asset('images/cookie_white_24dp.svg')"/>
            <div class="text">{!!$cookie_message!!}</div>
            <a class="button cookie-button" href="#"><span><?= __('Accept Cookies', 'sage') ?></span></a>
        </div>
    </div>
</footer>