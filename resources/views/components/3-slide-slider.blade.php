<div class="home-page-slider">
    <div class="home-slider grid-x grid-margin-x content-medium">
        @if($slides)
            @foreach ($slides as $slide)
                @include('components.slide',[
                    'title' => get_the_title($slide->ID),
                    'content' => App::get_post_excerpt($slide, 50),
                    'date' => get_the_date('F j, Y', $slide->ID),
                    'link' => get_permalink($slide->ID),
                    'read_more' => get_field('read_more_button', $slide->ID),
                ])
            @endforeach
        @endif
    </div>
</div>