export default function () {
    function init() {

        const $dropdown = $('.expandable-block__link');

        $dropdown.on('click', function(event){
            event.preventDefault();
            $(this).siblings().toggleClass('active');
            $(this).toggleClass('active');
        })
    }
    return { init };
}
