<!-- This file is used to build expandable blocks
Material Icons are use when expanding the block - active
You could change the type of material icon using the line below 
And specify the active and inactive icons to be used-->
<?php $material_icon = 'sharp';
$expandable_button_active = 'add';
$expandable_button_inactive = 'remove';?>


@if(!empty($expandables))
<div class="expandables-component">
    <div class="expandables grid-container grid-x">
        <div class="expandables-col1 cell large-6 medium-12 small-12">
            @if(!empty($overline_text))
            <span class="overline">{!!$overline_text!!}</span>
            @endif
            @if(!empty($main_header))
            <h2>{!!$main_header!!}</h2>
            @endif
        </div>
        <div class="expandables-col2 cell large-6 medium-12 small-12">
        @foreach($expandables as $expandable)
            <div class="expandable">
                <div class="expandable-block__link">
                    <div class="expandable__title grid-x">
                        <div class="expandable__heading cell large-10 small-10">
                            <h6>{!!$expandable['header'] ?? ''!!}<h6>
                        </div>
                        <div class="expandable__button-active expandable__button">
                            <span class="material-icons-{!!$material_icon!!}">
                                {!!$expandable_button_active!!}
                            </span>
                        </div>
                        <div class="expandable__button-inactive expandable__button">
                            <span class="material-icons-{!!$material_icon!!}">
                            {!!$expandable_button_inactive!!}
                            </span>
                        </div>
                    </div>
                </div>
                <div class="expandable__content">
                {!!$expandable['contents'] ?? ''!!}
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>
@endif