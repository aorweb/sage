@extends('layouts.app')

@section('content')
  <div class="grid-container">
    @include('partials.page-header')
    @if (!have_posts())
      <div class="alert alert-warning">
        {{ __('Sorry, no results were found.', 'sage') }}
      </div>
      {!! get_search_form(false) !!}
    @endif

    <div class="archive__card-container grid-x grid-margin-x">
      @while (have_posts()) @php(the_post())
        <div class="cell small-12 medium-6 large-3">
          {{-- @include('content.content-'.get_post_type()) --}}
          @include('components.date-image-card', [
            'link' => get_the_permalink(),
            'image' => get_the_post_thumbnail_url(),
            'title' => get_the_title(),
            'listings' => get_the_terms($post->id, 'taxonomies'),
            'date' => get_the_date('t/n/Y'),
          ])
        </div>
      @endwhile
    </div>

    @include('partials.pagination')
  </div>
@endsection
