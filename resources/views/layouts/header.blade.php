<header class="header">
  <div class="header__inner grid-container">
    @if($header_logo)
      <div class="header__logo-container">
        <a class="brand" href="{{ home_url('/') }}">
          <img src="{{ $header_logo }}" alt="{{ $site_name }}" />
        </a>
      </div>
    @endif
    
    <div class="header__menu">
      @if (has_nav_menu('menu'))
        {!! App\menu() !!}
      @endif
    </div>
    <div class="header__actions">
        <a href="/" class="menu-toggle">
          @include('components.menu-icon')
        </a>
    </div>
  </div>
</header>

@include('partials.site-nav')