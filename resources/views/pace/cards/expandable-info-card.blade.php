<div class="expandable-info-card cell small-12 large-6">
    <div class="expandable-info-card__inner grid-x grid-margin-x">
        @if($image)
            <div class="expandable-info-card__image cell small-12 large-6"
                style="background-image: url('{{$image}}')">
            </div>
        @endif
        <div class="expandable-info-card__content cell small-12 {{$image ? 'large-6' : 'large-12'}}">
            <h4 class="expandable-info-card__name">{!!$name ?? ''!!}</h4>
            <h6 class="expandable-info-card__position">{!!$position ?? ''!!}</h6>
            <div class="expandable-info-card__number"><caption>{!!$number ?? ''!!}</caption></div>
            <div class="expandable-info-card__email"><caption>{!!$email ?? ''!!}</caption></div>
            @if($linkedin)
                <div class="expandable-info-card__linkedin">
                    <a href="{!!$linkedin!!}">
                        <img src="@asset('images/linkedin-icon.svg')"/>
                    </a>
                </div>
            @endif
            @if($description)
                <div class="expandable-info-card__more-container">
                    <a href="" class="expandable-info-card__more">
                        <span class="more"><?= __('More', 'sage') ?></span>
                        <span class="material-icons-outlined">
                            expand_more
                        </span>
                    </a>
                </div>
            @endif
        </div>
    </div>
    @if($description)
        <div class="expandable-info-card__description">
            {!!$description!!}
        </div>
    @endif
</div>