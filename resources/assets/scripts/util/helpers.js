/**
 * Calculate the width of the scroll bar on the page
 *
 * @return {int}  the scroll bar width in px
 */
function getScrollBarWidth() {
    // Return 0 if there is no scrollbar
    if (document.body.scrollHeight <= window.innerHeight) {
        return 0;
    }

    // Create outer element with overflow scroll and inner element within it with 100% width
    let $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();

    $outer.remove();

    // Return scroll bar width by subtracting the inner element's width from the outer element's width
    return 100 - widthWithScroll;
}

/**
 * Toggles the ability to scroll the page body. Offsets elements to account for the removed sidebar
 *
 * @param {boolean} makeScrollable  should the body be made scrollable or not
 * @param {jQuery object} $body     the body HTML element
 * @param {array} padElements       an array of HTML elements that should receive padding offset
 * @param {string} className        a class name that can be used to toggle the body's state
 */
function toggleBodyScrollable(makeScrollable = null, $body = false, padElements = false, className = false) {
    $body = $body || $('body');

    if (makeScrollable === null) {
        makeScrollable = className ? $body.hasClass(className) : $body.css('overflow') === 'hidden';
    }

    // Toggle class
    if (className) {
        $body.toggleClass(className);
    }
    else {
        $body.css('overflow', makeScrollable ? 'auto' : 'hidden');
    }

    if (!makeScrollable) {
        let scrollBarWidth = getScrollBarWidth();

        if (scrollBarWidth > 0) {
            // Add padding to offset for missing scrollbar
            if (padElements && padElements.length > 0) {
                padElements.forEach(function ($e) {
                    $e.css('padding-right', scrollBarWidth);
                });
            }
        }
    }
    else {
        // Remove padding used to offset for scrollbar
        if (padElements && padElements.length > 0) {
            padElements.forEach(function ($e) {
                $e.css('padding-right', 0);
            });
        }
    }
}

/**
 * Calculates the trigger value in px for the specified breakpoint
 *
 * @param {string} size  the breakpoint identifier, e.g. small, medium, large
 * @return {int}         the px value of where the breakpoint triggers
 */
function getMediaQueryInt(size) {
    const pattern = /(\d*(?=em))/,
        baseFontSize = 16,
        querySize = window.Foundation.MediaQuery.get(size);

    return querySize ? (parseInt(querySize.match(pattern)) * baseFontSize) : null;
}

function getURLParam(name) {
    let param = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(location.search);

    return param ? decodeURIComponent(param[1]) : undefined;
}

export { getScrollBarWidth, toggleBodyScrollable, getMediaQueryInt, getURLParam }
