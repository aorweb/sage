import 'slick-carousel';
import 'jquery';

export default function () {
    function init() {
        $('.home-slider').slick({
            // speed: 500,
            arrows: false,
            prevArrow: "<span class='arrow-left arrow material-icons'>arrow_back</span>",
            nextArrow: "<span class='arrow-right arrow material-icons'>arrow_forward</span>",
            mobileFirst: true,
            centerMode: true,
            fade: false,
            infinite: true,
            easing: 'linear', //default
            adaptiveHeight: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            swipeToSlide: true,
            variableWidth: true,
            dots: true,
            responsive: [
                {
                    breakpoint: 1370,
                    settings: {
                        arrows: true,
                    },
                },
            ],

        })
    }
    return { init };
}
