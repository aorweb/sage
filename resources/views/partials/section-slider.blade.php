<div class="solutions-grid grid-container">
<div class="section-slider">
    <div class="section-content grid-x grid-margin-x">
        @foreach(get_field('section_slider') as $section_slider)
        <div class="section-cell cell large-4 medium-12 small-12">
            <div class="section-image">
                <img src="{{$section_slider['section_content']}}">
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>