<div class="date-image-card">
    <a href="{!! $link !!}">
        <div class="date-image-card__inner">
            <div class="date-image-card__image" @if($image) style="background-image: url('{!! $image !!}')" @endif></div>
            <div class="date-image-card__content">
                <div class="date-image-card__date-tax">
                    <div class="date-image-card__date">
                        <div class="date-image-card__icon">
                            <span class="material-icons">schedule</span>
                        </div>
                        <div class="date-image-card__contents_date">
                            <span class="date">{!! $date !!}</span>
                        </div>
                    </div>
                    @if($listings)
                        <div class="date-image-card__tags">
                            @foreach($listings as $listing)
                            <div class="date-image-card__tags-container">
                                <div class="date-image-card__listing">
                                    <p class="listing"> {!! $listing->name !!} </p> 
                                </div>
                            </div>
                            @endforeach
                        </div>
                    @endif
                </div>
                <h4 class="date-image-card__title">
                    {!! $title ?? '' !!}
                </h4>
            </div>
        </div>
    </a>
</div>