export default function() {
    function init() {

        $(window).resize(function(){

            if(window.innerWidth < 1022){
                $('.is-accordion-submenu').removeClass('xclose');

            } else {
                $('.is-accordion-submenu').addClass('xclose');
                $('.xclose').on('click', function(){
                    //event.preventDefault();
                    $('.submenu').hide();

                    if($('.submenu-toggle').attr('aria-expanded') == 'true') {
                        $('.submenu-toggle').attr('aria-expanded','false');
                    }
                })
            }
        });

        $(window).load(function(){

            if(window.innerWidth < 1022){
                $('.is-accordion-submenu').removeClass('xclose');

            } else {
                $('.is-accordion-submenu').addClass('xclose');
                $('.xclose').on('click', function(){
                    //event.preventDefault();
                    $('.submenu').hide();

                    if($('.submenu-toggle').attr('aria-expanded') == 'true') {
                        $('.submenu-toggle').attr('aria-expanded','false');
                    }
                })
            }
        });
    }
    return { init };
}