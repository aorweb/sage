<footer class="footer">
  <div class="footer__inner grid-container grid-x">
    <div class="footer_menus footer_column cell large-4 medium-12">
      <div class="footer_title">
            <span class="title">
              MENU
            </span>
        </div>
        <div class="menu grid-x">
          @if (has_nav_menu('footer_menu'))
            {!! App\footer_menu() !!}
          @endif
        </div>
    </div>

    <div class="footer-bottom cell large-12 medium-12 grid-x">
        <div class="copy-notice cell large-4 medium-12">
          <span><?php $year = date("Y"); ?>© {!!$year!!} {{ $site_name }} | All rights reserved.</span>
        </div>

        @if($privacy_policy)
        <div class="copy-notice cell large-8 medium-12"> 
          <a href="{!!$privacy_policy['url']!!}" target="{!!$privacy_policy['target']!!}">{!!$privacy_policy['title'] ?? 'Privacy Policy'!!}
          </a>
        </div>
        @endif
      </div>
  </div>
</footer>
