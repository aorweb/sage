/**
 * Component: Search
 *
 * This component handles the toggling of the search panel.
 */

import Panel from '../components/Panel';

export default function() {
    // Define constants
    const $search = $('.site-nav--search'),
          $btn = $('.search-toggle');

    // Create search panel and assign callback function
    let searchPanel = Panel($search, $btn);

    return {
        toggle: searchPanel.toggle,
        isActive: searchPanel.isActive,
        addCallback: searchPanel.addCallback,
    };
}