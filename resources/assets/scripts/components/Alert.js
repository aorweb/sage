/**
 * Component: Callout
 *
 * This component handles setting the cookies needed to hide the site callout element.
 */

 export default function() {

    function init() {
        $(window).on('load resize', function () {
            var $alert_height = $('.bottom-alert').outerHeight();
            if($('.bottom-alert').is(":visible")) {
                $('.footer').css('margin-bottom', $alert_height)
            }
            // if ($callout.length > 0) {
                $('.alert-close').on('click', setCookies);
            // }
        })
    }
    function setCookies() {
        document.cookie = 'alert_hidden=true;path=/';
        $('.footer').css('margin-bottom', '0')
        // document.cookie = 'alert_id=' + $callout.attr('id') + ';path=/';
    }
    
    // $('.bottom-alert__info').on('click', function(){

    //     $('.bottom-alert').hide();
    //   })
    $('.bottom-alert__info').on('click', function(){
        $('.bottom-alert').css('display','none');
    });
    return { init };
}
