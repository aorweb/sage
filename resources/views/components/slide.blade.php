<div class="home-slide cell small-12 medium-6 large-4">
    <div class="home-slide__container">
        @if(!$date && !$content)
            <div class="home-slide__image" style="{{$image ? 'background-image: url('.$image.')' : 'background-color: #1871cb'}}"></div>
        @endif
        <div class="home-slide__inner">
            <div class="home-slide__title-container">
                <h4>{{$title ?? ''}}</h4>
            </div>
            @if($date)
                <div class="home-slide__date">
                    <span>Published: {{$date}}</span>
                </div>
            @endif
            @if($content)
                <div class="home-slide__content">
                    {!! $content ?? '' !!}
                </div>
            @endif
            @if($read_more)
                <div class="home-slide__button-container">
                    <a class="button" href="{{ $link ?? '' }}" target="{{ $target }}">{{__('Learn More','sage')}}</a>
                </div>
            @endif
        </div>
    </div>
</div>
