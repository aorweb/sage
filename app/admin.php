<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);

    // Add custom logo support
    $wp_customize->add_section('theme_images_section', [
        'title' => __('Theme Branding Images', 'sage'),
        'priority' => 30,
        'description' => __('Add logos and branding to your theme.', 'sage'),
    ]);

    $wp_customize->add_setting('header_logo', ['transport' => 'postMessage']);
    $wp_customize->add_control(new \WP_Customize_Image_Control($wp_customize, 'header_logo', [
        'label' => __('Header Logo', 'sage'),
        'section' => 'theme_images_section',
        'settings' => 'header_logo',
    ]));
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});
