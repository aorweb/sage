import tagManager from './util/tagManager';

if (window.gtmID) {
    tagManager.init(window.gtmID[0]);
}
