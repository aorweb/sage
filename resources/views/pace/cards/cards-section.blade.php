<!-- If the card feild is empty this card section will not get displyed -->
@if(!empty(get_field('section_slider')))

<!-- Cards background class-->
<div class="card-background">

    <!-- Cards container-->
    <div class="card-container grid-container">
        <div class="cards grid-container grid-x">

            <!-- card section header -->
            @if(!empty(get_field('section_title')))
            <div class="card--header cell large-12 small-12">
                <h1 class="card__heading">
                    {!!get_field('resource_library_header_text')!!}
                </h1>
            </div>
            @endif

            <!-- card section view all link -->
            @if(!empty(get_field('section_link')))
            <div class="view-all cell large-12 small-12">
                <a class="view-all_link" href="{!!$resource_library_overline_text['url']!!}" target="{!!$resource_library_overline_text['target']!!}">{!!$resource_library_overline_text['title']!!}</a>
            </div>
            @endif

            <!-- cards content -->
            <div class="cards--contents grid-x grid-margin-x">
            @foreach(get_field('section_slider') as $section_slider)
                <a href="{!!$resource['resource_link']!!}" class="cards_contents cell large-3 medium-6 small-12">
                    <div class="card--contents_container">
                        <div class="card--contents_image">
                            <img class="cards--contents-image" src="{{$section_slider['section_content']}}">
                        </div>
                        <div class="cards--contents_body">
                            <div class="cards--contents_title">
                                <h4>{!!$resource['resource_title']!!}</h4>
                            </div> 
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif