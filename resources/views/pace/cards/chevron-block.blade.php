{{--
Card with option to add title link and image. Image will be displayed with the outline of a chevron
in the bottom right corner

Site: Pace Home page Program blocks

Components
$link - string:
$title - string:
$image - string:
 --}}
<div class="chevron-block cell small-12 large-4">
    <a href="{{$link}}">
        <div class="chevron-block__background-image"
            @if($block_count == 0)
                style="background-image: url(@asset('images/program-shape_blue_.svg'))"
            @elseif($block_count == 1)
                style="background-image: url(@asset('images/program-shape_grey_.svg'))"
            @elseif($block_count == 2)
                style="background-image: url(@asset('images/program-shape_green_.svg'))"
            @endif>
            @if($image)
                <img class="chevron-block__featured-image" src="{!!$image!!}"/>
            @endif
        </div>

        <div class="chevron-block__inner">
            <div class="chevron-block__heading-container">
                <h1 class="heading">
                    {!!$title!!}
                </h1>
            </div>
            <div class="chevron-block__link-container">
                @include('partials.buttons.arrow-link', [
                    'link' => $link
                ])
            </div>
        </div>
    </a>
</div>