export default function() {
    function init() {
        const $menu_button = $('.submenu-toggle');

        //deal with header menu active states.
        $menu_button.on('click', function() {
            if($('.submenu').hasClass('is-active')) {
                $('.submenu').hide();
                $('.submenu-toggle').removeClass('active');
                $('.menu-item').removeClass('active');
                $('.menu-item').removeClass('menu-active');
                $('.menu').removeClass('is-active');
                $('.submenu-toggle').attr("aria-expanded","false");

                $(this).siblings('.menu').addClass('is-active');
                $(this).siblings('.submenu').show();
                $(this).parent().addClass('active');
                $(this).addClass('active');
                $(this).attr("aria-expanded","true");
            } else {
                $(this).siblings('.submenu').hide();
                $('.submenu-toggle').removeClass('active');
                $(this).parent(). removeClass('active');
            }
        })

        $('.submenu-toggle').on('click', function() {
            if($(this).attr('aria-expanded') == 'true') {
                $(this).parent().addClass('menu-active');
            }else {
                $(this).parent().removeClass('menu-active');
            }
        });

        //hide site menu and socials on button click for a second so there isn't any weird animation going on while the menu animation is going
        $('.menu-toggle').on('click', function() {
            $('.site-nav').children('.menu').hide();
            $('.site-nav').children('.site-nav__social').hide();
            
            setTimeout(function() {
                $('.site-nav').children('.menu').show();
                $('.site-nav').children('.site-nav__social').show();
            }, 500);   
        });

        // $('.header__menu').find('.is-accordion-submenu').css('border','1px solid red');
    }
    return { init };
}