
<div class="block-expandable cell">
    <div class="block-expandable__title">
        <a href="" class="block-expandable__link">
            <h3>{!! $title !!}</h3>
            <span class="material-icons">
                arrow_forward_ios
            </span>
        </a>
    </div>
    <div class="block-expandable__excerpt hidden">
        <div class="block-expandable__content">
            {!! $content !!}
        </div>
        @if(!empty($menu) && isset($menu))
            <div class="block-expandable__menu">
                @foreach($menu as $menu_item)
                    <div class="block-expandable__menu-item">
                        <img class="leaderboard" src="@asset('images/leaderboard_black.svg')"/>
                        <a href="{!!$menu_item['menu_link']['link']!!}" target="{!!$menu_item['menu_link']['target']!!}">{!!$menu_item['menu_link']['title']!!}</a>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>
