<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class())>
    <noscript>
      @include('partials.no-script')
    </noscript>
    @php(do_action('get_header'))
    @include('layouts.header')
    <div class="wrap container" role="document">
      <div class="content">
        <main class="main column small-12 {{ App\display_sidebar() ? 'large-8' : '' }}">
          @yield('content')
        </main>
        @if(App\display_sidebar())
          <aside class="sidebar column large-4 show-for-large">
            @include('partials.sidebar-'.((get_post_type() !== 'page') ? get_post_type() : get_post()->post_name))
          </aside>
        @endif
      </div>
    </div>
    @php(do_action('get_footer'))
    @include('layouts.footer')

    @php(wp_footer())
    
    @if(!empty(get_field('alert_text','theme_options')))
    <div class="bottom-alert {{!empty($_COOKIE['alert_hidden']) ? $_COOKIE['alert_hidden'] : ''}}" data-closable>
      <div class="bottom-alert__inner grid-container">
          <div class="cookie-header">
            <div class="cookie-header_text">
                <h3>Cookie Consent</h3>
            </div>
            <div class="bottom-alert__info">
                <img src="@asset('images/close.svg')">
            </div>
          </div>
            <div class="content">
                <p>{!! get_field('alert_text','theme_options') !!}</p>
            </div>
            
        </div>
        
        <div class="bottom-alert__buttons grid-container">
          <div class="bottom-alert__button">
          <a class="button solid botttom-alert__exit alert-close" type="button" data-close>
          ACCEPT COOKIES</a>
          </div>

          <div class="bottom-alert__button">
          <a class="button transparent botttom-alert__exit alert-close" type="button" data-close>
          DECLINE COOKIES</a>
          </div>
        </div>
      </div>
    @endif
  </body>
</html>
