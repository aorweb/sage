{{--
  Template Name: Test Template
--}}

@extends('layouts.app')

@section('content')
    <div class="grid-container">
        @while(have_posts()) @php(the_post())
            @include('partials.page-header')
            @include('content.content-page')
            @include('components.3-slide-slider',[
                'slides' => get_posts(),
            ])
        @endwhile
    </div>
@endsection
