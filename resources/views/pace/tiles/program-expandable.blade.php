<div class="program-expandable">
    <div class="program-expandable__inner">
        <div class="program-expandable__title">
            <div class="program-expandable__icon">
                @if($icon)
                    <img class="img" src="{!!$icon!!}"/>
                @endif
                <h4 class="heading">{!!$title ?? ''!!}</h4>
            </div>
            <a href="" class="information-expandable-tile__link program-open"><span class="material-icons">expand_more</span></a>
        </div>
        <div class="program-expandable__expandable">
            @if($button)
                <a class="button" href="{!!$button['url']!!}" target="{!!$button['target']!!}"><span>{!!$button['title']!!}</span></a>
            @endif
            @if($image)
                <img class="image" src="{!!$image!!}"/>
            @endif
            <span class="desc">{!!$desc ?? ''!!}</span>

            @if($numbers)
                @foreach($numbers as $i => $number)
                    <div class="program-expandable__number">
                        <div class="number-container">
                            <h1 id="numbers-{{$i}}" class="number" data-number="{{$number['number']}}"></h1>
                            @if(!empty($number['number_after_text']))
                                <h1>{!!$number['number_after_text']!!}</h1>
                            @endif
                        </div>
                        <div class="sub-text">
                            <span class="material-icons">check_circle</span>
                            <span class="sub-text">{!!$number['sub_text']!!}</span>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
